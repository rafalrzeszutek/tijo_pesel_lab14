package pl.edu.pwsztar

import spock.lang.Specification

class UserIdSpec extends Specification {

    def idArray = ['53100335585', '01042707562', 'abcdefghijk', '']

    def "should check correct size"() {
        given:
        UserId id1 = new UserId(idArray.get(0))
        UserId id2 = new UserId(idArray.get(1))
        UserId id3 = new UserId(idArray.get(2))
        UserId id4 = new UserId(idArray.get(3))
        when:
        def answer = [id1.isCorrectSize(),
                      id2.isCorrectSize(),
                      id3.isCorrectSize(),
                      id4.isCorrectSize(),]
        then:
        answer == [true,true,true,false]
    }

    def "should check valid sex"(){
        given:
        UserId id1 = new UserId(idArray.get(0))
        UserId id2 = new UserId(idArray.get(1))
        UserId id3 = new UserId(idArray.get(2))
        UserId id4 = new UserId(idArray.get(3))
        when:
        def answer = [(id1.getSex().toString() == "Optional[WOMAN]"),
                      id2.getSex().toString() == "Optional[WOMAN]",
                      id3.getSex().toString() == "Optional.empty",
                      id4.getSex().toString() == "Optional.empty"]
        then:
        answer == [true, true, true, true]
    }

    def "should check valid id"(){
        given:
        UserId id1 = new UserId(idArray.get(0))
        UserId id2 = new UserId(idArray.get(1))
        UserId id3 = new UserId(idArray.get(2))
        UserId id4 = new UserId(idArray.get(3))
        when:
        def answer = [id1.isCorrect(),
                      id2.isCorrect(),
                      id3.isCorrect(),
                      id4.isCorrect()]
        then:
        answer == [true,true,false,false]
    }

    def "should return date"(){
        given:
        UserId id1 = new UserId(idArray.get(0))
        UserId id2 = new UserId(idArray.get(1))
        UserId id3 = new UserId(idArray.get(2))
        UserId id4 = new UserId(idArray.get(3))
        when:
        def answer = [(id1.getDate() == Optional.of('03-10-1953')),
                      id2.getDate() == Optional.of('27-04-1901'),
                      id3.getDate() == Optional.empty(),
                      id4.getDate() == Optional.empty()]
        then:
        answer == [true, true, true, true]
    }
}

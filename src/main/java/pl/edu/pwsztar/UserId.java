package pl.edu.pwsztar;

import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        if(id.length() == 11)
            return true;
        else
            return false;
    }

    @Override
    public Optional<Sex> getSex() {
        if(isCorrect())
        {
            int[] arr = getIdIntArray();
            Sex sex = arr[9] % 2 == 0 ? Sex.WOMAN : Sex.MAN;
            return Optional.of(sex);
        }
        return Optional.empty();
    }

    @Override
    public boolean isCorrect() {
        boolean ifIdMatches = id.matches("^[0-9]*$");

        if(id.length() == 11 && ifIdMatches){
            int[] sumControlNumbers = {9,7,3,1,9,7,3,1,9,7};
            int sum=0;
            int[] idArray = getIdIntArray();

            for (int i = 0; i < sumControlNumbers.length; i++) {
                sum += idArray[i] * sumControlNumbers[i];
            }
            return sum % 10 == idArray[10];
        }
        return false;
    }

    public int[] getIdIntArray() {
        String[] split = id.split("");
        int[] idArray = new int[split.length];
        for (int i=0; i < split.length; i++) {
            idArray[i] = Integer.parseInt(split[i]);
        }
        return idArray;
    }

    @Override
    public Optional<String> getDate() {
        if(isCorrect()){
            return Optional.of(getFullDate());
        }
        return Optional.empty();
    }

    private String getFullDate() {
        int[] idInts = getIdIntArray();
        int year = (idInts[0] * 10) + idInts[1];

        int month = (idInts[2] * 10) + idInts[3] ;

        if (month > 80 && month < 93) {
            year += 1800;
        }
        else if (month > 0 && month < 13) {
            year += 1900;
        }
        else if (month > 20 && month < 33) {
            year += 2000;
        }
        else if (month > 40 && month < 53) {
            year += 2100;
        }
        else if (month > 60 && month < 73) {
            year += 2200;
        }

        return getDay((idInts[4] * 10) + idInts[5]) + "-" + getMonth(month) + "-" +year;
    }

    private String getMonth(int month) {
        if (month > 80 && month < 93) {
            month -= 80;
        }
        else if (month > 20 && month < 33) {
            month -= 20;
        }
        else if (month > 40 && month < 53) {
            month -= 40;
        }
        else if (month > 60 && month < 73) {
            month -= 60;
        }
        if(month<10){
            return '0'+String.valueOf(month);
        }
        return String.valueOf(month);
    }

    private String getDay(int day) {
        if(day<10){
            return '0'+String.valueOf(day);
        }
        return String.valueOf(day);
    }
}
